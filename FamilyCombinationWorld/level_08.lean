have h2 : ⋃₀ {{x} | x ∈ A} = A := by
  ext x
  apply Iff.intro
  · intro h
    cases' h with X h3
    have h4 := h3.left
    have ⟨x', ⟨h5, h6⟩⟩ := h4
    have h7 := h3.right
    rw [← h6, single_def] at h7
    rw [h7]
    exact h5
  · intro h2
    use {x}
    apply And.intro
    · use x
    · rw [single_def]
have h3 := h1 {{x} | x ∈ A}
have h4 := h3 h2
have ⟨x, h5⟩ := h4
use x
exact h5.right.symm
